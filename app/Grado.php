<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table = 'grados';

    protected $fillable = [
        'nombre','profesor_id'
    ];

    public function profesor(){
        return $this->belongsTo('App\Profesor');
    }

    public function alumnos(){
        return $this->belongsToMany('App\Alumno');
    }
    
 
}
