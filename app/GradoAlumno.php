<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradoAlumno extends Model
{
    protected $table = 'alumno_grado';

    protected $fillable = [
        'seccion','alumno_id','grado_id'
    ];

    public function relacion(){
        return $this->belongsToMany('App\GradoAlumno');
    }
    
    public function asociarAlumno(){
        
    }
    
}
