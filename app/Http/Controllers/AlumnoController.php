<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Http\Requests\AlumnoRequest;

class AlumnoController extends Controller
{
    public function index(){
        $alumnos = Alumno::orderBy('nombre','ASC')->paginate(5);
        $alumnos->each(function($alumnos){
            $alumnos->grados;
            //dd($alumnos->grados);
            
        });
        return view('alumno.index')->with('alumnos',$alumnos);
    }

    public function create(){
        return view('alumno.create');
    }

    /**
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Respone
     */
    public function store(AlumnoRequest $request){
        $alumno = new Alumno($request->all());
        $alumno->save();


        flash('Se ha creado el Alumno ' . $alumno->name.' de forma exitosa!')->success()->important();
        return redirect()->route('alumno.index');
    }

    public function destroy($id){
        $alumno = Alumno::find($id);
        $alumno->delete();

        flash('Se ha eliminado el Alumno ' . $alumno->name.' de forma exitosa!')->error()->important();
        return redirect()->route('alumno.index');
    }

    public function edit($id){
        $alumno = Alumno::find($id);
        return view('alumno.edit')->with('alumno',$alumno);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @param int $id
     * @return Illuminate\Http\Respone
     */
    public function update(AlumnoRequest $request, $id)
    {
        $alumno = Alumno::find($id);
        $alumno->fill($request->all());
        $alumno->save();   

        flash('Se ha editado el Alumno ' . $alumno->name.' de forma exitosa!')->success()->important();
        return redirect()->route('alumno.index');
    }
}
