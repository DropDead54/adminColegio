<?php

namespace App\Http\Controllers;
use App\Http\Requests\GradoRequest;
use Illuminate\Http\Request;
use App\Grado;
use App\Profesor;

class GradosController extends Controller
{
    public function index(){
        $grados = Grado::orderBy('nombre','ASC')->paginate(5);
        $grados->each(function($grados){
            $grados->profesor;
        });

        return view('grado.index')->with('grados',$grados);
    }

    public function create(){
        $profesores = Profesor::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('grado.create')->with('profesores',$profesores);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Respone
     */
    public function store(GradoRequest $request){
        
        $bandera = false;
        $grado = new Grado($request->all());

        $profesor = Profesor::find($grado->profesor_id);

        $grados = Grado::all();
        foreach($grados as $grad){

            if($grad->profesor_id == $profesor->id){
                $bandera = true;
            }
        }
        if($bandera == true){
            flash('El Profesor ya fue asignado a un Curso')->success()->important();
            return redirect()->route('grado.index');
        }else{
            $grado->save();
            flash('Se ha creado el Grado ' . $grado->nombre.' de forma exitosa!')->success()->important();
            return redirect()->route('grado.index');
        }
        


        
    }

    public function destroy($id){
        $grado = Grado::find($id);
        $grado->each(function($grado){
            $grado->alumnos;
            
        });
       
        if($grado->alumnos){
            flash('No se puede eliminar el Curso porque tiene Alumnos asignados')->error()->important();
            return redirect()->route('grado.index');
        }else{
            $grado->delete();
            flash('Se ha eliminado el grado ' . $grado->nombre.' de forma exitosa!')->error()->important();
             return redirect()->route('grado.index');
        }
        

        
    }

    public function edit($id){
        $grado = Grado::find($id);
        $profesores = Profesor::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('grado.edit')
            ->with('grado',$grado)
            ->with('profesores',$profesores);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @param int $id
     * @return Illuminate\Http\Respone
     */
    public function update(GradoRequest $request, $id)
    {
        $grado = Grado::find($id);
        $grado->fill($request->all());
        $grado->save();   

        flash('Se ha editado el Grado ' . $grado->nombre.' de forma exitosa!')->success()->important();
        return redirect()->route('grado.index');
    }
}
