<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesor;
use App\Http\Requests\ProfesorRequest;

class ProfesorController extends Controller
{
    public function index(){
        $profesores = Profesor::orderBy('nombre','ASC')->paginate(5);
        return view('profesor.index')->with('profesores',$profesores);
    }

    public function create(){
        return view('profesor.create');
    }

    /**
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Respone
     */
    public function store(ProfesorRequest $request){
       
        $profesor = new Profesor($request->all());
        $profesor->save();


        flash('Se ha creado el Profesor ' . $profesor->name.' de forma exitosa!')->success()->important();
        return redirect()->route('profesor.index');
    }

    public function destroy($id){
        $profesor = Profesor::find($id);
        $profesor->delete();

        flash('Se ha eliminado el Profesor ' . $profesor->name.' de forma exitosa!')->error()->important();
        return redirect()->route('profesor.index');
    }

    public function edit($id){
        $profesor = Profesor::find($id);
        return view('profesor.edit')->with('profesor',$profesor);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @param int $id
     * @return Illuminate\Http\Respone
     */
    public function update(ProfesorRequest $request, $id)
    {
        $profesor = Profesor::find($id);
        $profesor->fill($request->all());
        $profesor->save();   

        flash('Se ha editado el Profesor ' . $profesor->name.' de forma exitosa!')->success()->important();
        return redirect()->route('profesor.index');
    }
}
