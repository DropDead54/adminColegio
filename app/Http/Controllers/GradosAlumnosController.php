<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GradoAlumnoRequest;
use App\GradoAlumno;
use App\Alumno;
use App\Grado;

class GradosAlumnosController extends Controller
{
    public function index(){
        $gradosAlumno = GradoAlumno::orderBy('id','ASC')->paginate(5);
              
        $alumnos = Alumno::all();
        $grados = Grado::all();

        return view('grado_alumno.index')
            ->with('gradosAlumno',$gradosAlumno)
            ->with('alumnos',$alumnos)
            ->with('grados',$grados);
    }

    public function create(){
        $alumnos = Alumno::orderBy('nombre','ASC')->pluck('nombre','id');
        $grados = Grado::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('grado_alumno.create')
            ->with('alumnos',$alumnos)
            ->with('grados',$grados);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Respone
     */
    public function store(GradoAlumnoRequest $request){
        
        $gradosAlumno = new GradoAlumno($request->all());
        $gradosAlumno->save();


        flash('Se ha creado el Grado con Alumno de forma exitosa!')->success()->important();
        return redirect()->route('grado_alumno.index');
    }

    public function destroy($id){
        $gradosAlumno = GradoAlumno::find($id);
        $gradosAlumno->delete();

        flash('Se ha eliminado el grado con Alumno de forma exitosa!')->error()->important();
        return redirect()->route('grado_alumno.index');
    }

    public function edit($id){
        $gradosAlumno = GradoAlumno::find($id);
        $alumnos = Alumno::orderBy('nombre','ASC')->pluck('nombre','id');
        $grados = Grado::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('grado_alumno.edit')
            ->with('gradosAlumno',$gradosAlumno)
            ->with('grados',$grados)
            ->with('alumnos',$alumnos);
    }

    /**
     * @param Illuminate\Http\Request $request
     * @param int $id
     * @return Illuminate\Http\Respone
     */
    public function update(GradoAlumnoRequest $request, $id)
    {
        $gradosAlumno = GradoAlumno::find($id);
        $gradosAlumno->fill($request->all());
        $gradosAlumno->save();   

        flash('Se ha editado el Grado con Alumno de forma exitosa!')->success()->important();
        return redirect()->route('grado_alumno.index');
    }
}
