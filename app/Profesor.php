<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table = 'profesores';

    protected $fillable = [
        'nombre','apellido','genero'
    ];

    public function grados(){
        return $this->hasMany('App\Grado');
    }
}
