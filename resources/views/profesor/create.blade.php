@extends('template.main')

@section('title','Crear Profesor') 

@section('content')
    {!! Form::open(['route' => 'profesor.store', 'method' => 'POST']) !!}
        
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',null ,[ 'class'=>'form-control', 'placeholder' => 'Nombre Completo', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellido','Apellido ') !!}
        {!! Form::text('apellido',null ,[ 'class'=>'form-control', 'placeholder' => 'Apellido Completo', 'required' ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('genero','Genero') !!}
        {!! Form::select('genero',['Hombre'=>'Hombre', 'Mujer'=>'Mujer'],null,[ 'class'=>'form-control', 'placeholder' => 'Selecciona una opción', 'required' ]) !!}
    </div>

    
    <div class="form-group">
        {!! Form::submit('Crear Profesor', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection