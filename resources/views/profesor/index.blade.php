@extends('template.main')

@section('title','Profesor') 

@section('content')
<br><a href="{{route('profesor.create')}}" class="btn btn-success"> Crear Nuevo Profesor</a><br><br>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Genero</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>
        
                @foreach($profesores as $profesor)
                <tr>
                    <th scope="row">{{$profesor->id}}</th>
                    <td>{{$profesor->nombre}}</td>
                    <td>{{$profesor->apellido}}</td>
                    <td>{{$profesor->genero}}</td>
                    <td>
                        
                        {!! Form::open(['route' => ['profesor.destroy',$profesor->id], 'method' => 'DELETE']) !!}
                            <a href="{{route('profesor.edit',$profesor->id)}}" class="btn btn-danger"><i class="fas fa-pencil-alt"></i></a> 
                            <button type="submit" class="btn btn-warning" onclick="return confirm('Seguro que quieres elimanar el tag {{$profesor->nombre}} ?')"><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                        
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>
</div>
<div class="text-center">
    {!! $profesores->render() !!}
</div>

@endsection