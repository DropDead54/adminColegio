@extends('template.main')

@section('title','Crear Grado') 

@section('content')
    {!! Form::open(['route' => 'grado.store', 'method' => 'POST']) !!}
        
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',null ,[ 'class'=>'form-control', 'placeholder' => 'Nombre Completo', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('profesor_id','Profesor') !!}
        {!! Form::select('profesor_id',$profesores,null,['class'=>'form-control','placeholder'=>'Selecion un Profesor', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Crear Grado', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection