@extends('template.main')

@section('title','Grados') 

@section('content')
<br><a href="{{route('grado.create')}}" class="btn btn-success"> Crear Nuevo Grado</a><br><br>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Profesor</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>
        
                @foreach($grados as $grado)
                <tr>
                    <th scope="row">{{$grado->id}}</th>
                    <td>{{$grado->nombre}}</td>
                    <td>{{$grado->profesor->nombre}}</td>
                    <td>
                        
                        {!! Form::open(['route' => ['grado.destroy',$grado->id], 'method' => 'DELETE']) !!}
                            <a href="{{route('grado.edit',$grado->id)}}" class="btn btn-danger"><i class="fas fa-pencil-alt"></i></a> 
                            <button type="submit" class="btn btn-warning" onclick="return confirm('Seguro que quieres elimanar el tag {{$grado->nombre}} ?')"><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                        
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>
</div>
<div class="text-center">
    {!! $grados->render() !!}
</div>

@endsection