@extends('template.main')

@section('title','Editar Grado' .$grado->nombre) 

@section('content')
    {!! Form::open(['route' => ['grado.update',$grado->id], 'method' => 'PUT']) !!}
        
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',$grado->nombre ,[ 'class'=>'form-control', 'placeholder' => 'Nombre Completo', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('profesor_id','Profesor') !!}
        {!! Form::select('profesor_id',$profesores,$grado->profesor_id,['class'=>'form-control ', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Editar Grado', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection