@extends('template.main')

@section('title','Asignacion de grado') 

@section('content')
    {!! Form::open(['route' => 'grado_alumno.store', 'method' => 'POST']) !!}
        
    <div class="form-group">
        {!! Form::label('seccion','Sección') !!}
        {!! Form::text('seccion',null ,[ 'class'=>'form-control', 'placeholder' => 'Sección', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('alumno_id','Alumno') !!}
        {!! Form::select('alumno_id',$alumnos,null,['class'=>'form-control','placeholder'=>'Selecion un Alumno', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('grado_id','Grado') !!}
        {!! Form::select('grado_id',$grados,null,['class'=>'form-control','placeholder'=>'Selecion un Grado', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Asignar Grado', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection