@extends('template.main')

@section('title','Asignacion de grado') 

@section('content')
    {!! Form::open(['route' => ['grado_alumno.update',$gradosAlumno->id], 'method' => 'PUT']) !!}
        
    <div class="form-group">
        {!! Form::label('seccion','Sección') !!}
        {!! Form::text('seccion',$gradosAlumno->seccion ,[ 'class'=>'form-control', 'placeholder' => 'Sección', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('alumno_id','Alumno') !!}
        {!! Form::select('alumno_id',$alumnos,$gradosAlumno->alumno_id,['class'=>'form-control','placeholder'=>'Selecion un Alumno', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('grado_id','Grado') !!}
        {!! Form::select('grado_id',$grados,$gradosAlumno->grado_id,['class'=>'form-control','placeholder'=>'Selecion un Grado', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Editar Asignación Grado', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection