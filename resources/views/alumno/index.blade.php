@extends('template.main')

@section('title','Alumnos') 

@section('content')
<h3>Lista de alumnos</h3>
<br><a href="{{route('alumno.create')}}" class="btn btn-success"> Crear Nuevo Alumno</a><br><br>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Genero</th>
                <th scope="col">Fecha de Nacimiento</th>
                <th scope="col">CursoAginado</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>
        
                @foreach($alumnos as $alumno)
                <tr>
                    <th scope="row">{{$alumno->id}}</th>
                    <td>{{$alumno->nombre}}</td>
                    <td>{{$alumno->apellido}}</td>
                    <td>{{$alumno->genero}}</td>
                    <td>{{$alumno->fecha_nacimiento}}</td>
                    <td>{{$alumno->grados}}</td>
                    <td>
                        
                        {!! Form::open(['route' => ['alumno.destroy',$alumno->id], 'method' => 'DELETE']) !!}
                            <a href="{{route('alumno.edit',$alumno->id)}}" class="btn btn-danger"><i class="fas fa-pencil-alt"></i></a> 
                            <button type="submit" class="btn btn-warning" onclick="return confirm('Seguro que quieres elimanar el tag {{$alumno->nombre}} ?')"><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                        
                    </td>
                </tr>
                @endforeach
        </tbody>
    </table>
</div>
<div class="text-center">
    {!! $alumnos->render() !!}
</div>

@endsection