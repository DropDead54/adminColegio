@extends('template.main')

@section('title','Crear Alumno') 

@section('content')
    {!! Form::open(['route' => 'alumno.store', 'method' => 'POST']) !!}
        
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',null ,[ 'class'=>'form-control', 'placeholder' => 'Nombre Completo', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellido','Apellido ') !!}
        {!! Form::text('apellido',null ,[ 'class'=>'form-control', 'placeholder' => 'Apellido Completo', 'required' ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('genero','Genero') !!}
        {!! Form::select('genero',['Hombre'=>'Hombre', 'Mujer'=>'Mujer'],null,[ 'class'=>'form-control', 'placeholder' => 'Selecciona una opción', 'required' ]) !!}
    </div>

    <div class="form-group">
            {!! Form::label('fecha_nacimiento','Fecha de Nacimiento ') !!}
            {!! Form::date('fecha_nacimiento',null ,[ 'class'=>'form-control', 'placeholder' => '00/00/00', 'required' ]) !!}
        </div>

    <div class="form-group">
        {!! Form::submit('Crear Alumno', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection