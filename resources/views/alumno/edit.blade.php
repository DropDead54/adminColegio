@extends('template.main')

@section('title','Editar Alumno ' . $alumno->nombre ) 

@section('content')
    {!! Form::open(['route' => ['alumno.update',$alumno->id], 'method' => 'PUT']) !!}
        
    <div class="form-group">
        {!! Form::label('nombre','Nombre') !!}
        {!! Form::text('nombre',$alumno->nombre ,[ 'class'=>'form-control', 'placeholder' => 'Nombre Completo', 'required' ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellido','Apellido ') !!}
        {!! Form::text('apellido',$alumno->apellido ,[ 'class'=>'form-control', 'placeholder' => 'Apellido Completo', 'required' ]) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('genero','Genero') !!}
        {!! Form::select('genero',['Hombre'=>'Hombre', 'Mujer'=>'Mujer'],$alumno->genero,[ 'class'=>'form-control', 'placeholder' => 'Selecciona una opción', 'required' ]) !!}
    </div>

    <div class="form-group">
            {!! Form::label('fecha_nacimiento','Fecha de Nacimiento ') !!}
            {!! Form::date('fecha_nacimiento',$alumno->fecha_nacimiento ,[ 'class'=>'form-control', 'placeholder' => '00/00/00', 'required' ]) !!}
        </div>

    <div class="form-group">
        {!! Form::submit('Editar Alumno', [ 'class'=>'btn btn-success']) !!}
    </div>

    {!! Form::close() !!}
@endsection